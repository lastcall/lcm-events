<?php
/**
 * @file
 * lcm_events.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function lcm_events_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create event content'.
  $permissions['create event content'] = array(
    'name' => 'create event content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
