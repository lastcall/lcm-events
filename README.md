### LCM Events ###

* This is an events-focused Drupal feature developed by Last Call Media for the College of Social and Behavioral Sciences at UMass Amherst.

### How do I get set up? ###

* Enable the LCM Events feature on a Drupal 7 site. If enabled with drush, all dependencies will also be downloaded and enabled.