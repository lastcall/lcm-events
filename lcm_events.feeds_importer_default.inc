<?php
/**
 * @file
 * lcm_events.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function lcm_events_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'events_importer';
  $feeds_importer->config = array(
    'name' => 'Events importer',
    'description' => 'Import event nodes from XML feed.',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserXML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'title',
          'xpathparser:4' => 'field_calendar_choices',
          'xpathparser:5' => 'field_event_category',
          'xpathparser:26' => 'field_event_submitter_name',
          'xpathparser:27' => 'field_event_submitter_email',
          'xpathparser:28' => 'field_event_submitter_phone',
          'xpathparser:8' => 'field_event_summary',
          'xpathparser:9' => 'body',
          'xpathparser:10' => 'field_event_website_title',
          'xpathparser:11' => 'field_event_website_url',
          'xpathparser:12' => 'field_event_contact_name',
          'xpathparser:13' => 'field_event_contact_phone',
          'xpathparser:14' => 'field_event_contact_email',
          'xpathparser:15' => 'field_event_place',
          'xpathparser:17' => 'field_event_location/premise',
          'xpathparser:18' => 'field_event_location/locality',
          'xpathparser:19' => 'field_event_location/administrative_area',
          'xpathparser:20' => 'field_event_location/postal_code',
          'xpathparser:21' => 'field_event_location/country',
          'xpathparser:22' => 'field_event_location/thoroughfare',
          'xpathparser:23' => 'field_event_date_time_start',
          'xpathparser:24' => 'field_event_date_time_end',
          'xpathparser:25' => 'guid',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:26' => 0,
          'xpathparser:27' => 0,
          'xpathparser:28' => 0,
          'xpathparser:8' => 0,
          'xpathparser:9' => 0,
          'xpathparser:10' => 0,
          'xpathparser:11' => 0,
          'xpathparser:12' => 0,
          'xpathparser:13' => 0,
          'xpathparser:14' => 0,
          'xpathparser:15' => 0,
          'xpathparser:17' => 0,
          'xpathparser:18' => 0,
          'xpathparser:19' => 0,
          'xpathparser:20' => 0,
          'xpathparser:21' => 0,
          'xpathparser:22' => 0,
          'xpathparser:23' => 0,
          'xpathparser:24' => 0,
          'xpathparser:25' => 0,
        ),
        'context' => '/nodes/node',
        'exp' => array(
          'errors' => 1,
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:26' => 0,
            'xpathparser:27' => 0,
            'xpathparser:28' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
            'xpathparser:12' => 0,
            'xpathparser:13' => 0,
            'xpathparser:14' => 0,
            'xpathparser:15' => 0,
            'xpathparser:17' => 0,
            'xpathparser:18' => 0,
            'xpathparser:19' => 0,
            'xpathparser:20' => 0,
            'xpathparser:21' => 0,
            'xpathparser:22' => 0,
            'xpathparser:23' => 0,
            'xpathparser:24' => 0,
            'xpathparser:25' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => 0,
          ),
          1 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_calendar_choices',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_event_category',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:26',
            'target' => 'field_event_submitter_name',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:27',
            'target' => 'field_event_submitter_email',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:28',
            'target' => 'field_event_submitter_phone',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_event_summary',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:9',
            'target' => 'body',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_event_website:title',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_event_website:url',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:12',
            'target' => 'field_event_contact_name',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:13',
            'target' => 'field_event_contact_phone',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:14',
            'target' => 'field_event_contact_email',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:15',
            'target' => 'field_event_place',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'xpathparser:17',
            'target' => 'field_event_location:premise',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'xpathparser:18',
            'target' => 'field_event_location:locality',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'xpathparser:19',
            'target' => 'field_event_location:administrative_area',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'xpathparser:20',
            'target' => 'field_event_location:postal_code',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'xpathparser:21',
            'target' => 'field_event_location:country',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'xpathparser:22',
            'target' => 'field_event_location:thoroughfare',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'xpathparser:23',
            'target' => 'field_event_date_time:lcm_events_start',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'xpathparser:24',
            'target' => 'field_event_date_time:lcm_events_end',
            'unique' => FALSE,
          ),
          22 => array(
            'source' => 'xpathparser:25',
            'target' => 'guid',
            'unique' => 1,
          ),
          23 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'full_html',
        'authorize' => 0,
        'skip_hash_check' => 0,
        'bundle' => 'event',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '3600',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['events_importer'] = $feeds_importer;

  return $export;
}
