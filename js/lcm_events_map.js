(function ($) {

Drupal.behaviors.eventMap = {
  attach: function(context, settings) {

	try {
	
	  // initialize a geocoder and retrieve the address text.
	  geocoder = new google.maps.Geocoder();
	  address = $("#gmap-addr").text();
	
	  // initialize a new google map in the provided div
	  map = new google.maps.Map(
		$("#google-map").get(0),
		{
		  zoom: 18,
		  mapTypeId: "roadmap",
		  minZoom: 1,
		  mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		  },
		  scrollwheel: false,
		  draggable: true,
		  scaleControl: false
		}
	  );
	
	  // geocode the address and use it to make a marker
	  geocoder.geocode( { address : address }, function(results, status) {
	
		if (status == google.maps.GeocoderStatus.OK) { // on success, render the returned information
		  position = results[0].geometry.location;
		  // center the map on the location
		  map.setCenter(position);
	
		  // make a new marker
		  marker = new google.maps.Marker({
			map: map,
			position: position
		  });
	
		  // make a new info window
		  content = '<div>';
		  content += $('.field-name-field-event-place .field-items .field-item').html();
		  content += $('.field-name-field-event-location .field-items .street-block').html();	
		  content += $('.field-name-field-event-location .field-items .locality-block').html();
		  content += '</div>';	
		  content += '<a href="http://maps.google.com/maps?daddr=' + encodeURIComponent(address) + '" target="_blank">Get directions</a>';
		  infowindow = new google.maps.InfoWindow({
			content: content,
			position: position
		  });
		  infowindow.open(map);
		} else { // on failure, hide the map
		  $("#google-map").hide();
		}
	  });
	
	} catch(err) {
	  $("#google-map").hide();
	}

  }
};

})(jQuery);
