(function($) {

Drupal.behaviors.calendarTooltip = {
  attach: function(context, settings) {

    $(".calendar.monthview", context).each(function() {
      $(".calendar.popup").hide();
      $(this).qtip({
        content: $(".calendar.popup", this).html(),
        position: {
          corner: {
            target: 'bottomMiddle',
            tooltip: 'topMiddle'
          }
        },
        show: 'click',
        hide: {
          when: 'unfocus',
          fixed: true,
        }
      })
    });
  }  
};


})(jQuery);