<?php


/**
 * Implements drush_MODULE_post_COMMAND().
 *
 * Download jquery.qtip and put it in sites/all/libraries.
 */
function drush_lcm_events_post_pm_enable() {
  $extensions = func_get_args();
  // Deal with comma delimited extension list.
  if (strpos($extensions[0], ',') !== FALSE) {
    $extensions = explode(',', $extensions[0]);
  }

  if (in_array('lcm_events', $extensions) && !drush_get_option('skip')) {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT');
    if($library_path = module_invoke('libraries', 'get_path', 'jquery.qtip')) {
      $path.= '/'  . $library_path;
    }
    else {
      $path .= '/sites/all/libraries/jquery.qtip';
    }

    if (is_dir($path) && file_exists($path . '/' . JQUERYQTIP_FILENAME)) {
      drush_log('jquery.qtip already present. No download required.', 'ok');
    }
    else {
      drush_log('made it here');
      if(file_prepare_directory(dirname($path), FILE_CREATE_DIRECTORY|FILE_MODIFY_PERMISSIONS)) {
        $request = drupal_http_request(JQUERYQTIP_DOWNLOAD);
        if($request->code == 200 && !empty($request->data)) {
          $folder_path = drush_tempdir();
          $filename = pathinfo(JQUERYQTIP_DOWNLOAD, PATHINFO_FILENAME);
          file_put_contents("$folder_path/$filename", $request->data);
          drush_tarball_extract("$folder_path/$filename", $folder_path);
          if(drush_move_dir($folder_path . '/qTip1-master/1.0.0-rc3', $path, TRUE)) {
            drush_log('jquery.qtip jquery plugin downloaded to ' . $path, 'ok');
            return;
          }
        }
      }
      $message = dt('jQuery jquery.qtip plugin download failed.  Please download the plugin from !link and move the plugin file (!script) to @path', array(
        '!script' => JQUERYQTIP_FILENAME,
        '!link' => JQUERYQTIP_DOWNLOAD,
        '@path' => $path . '/' . JQUERYQTIP_FILENAME,
      ));
      drush_log($message, 'warning');
    }
  }
}