<?php
/**
 * @file
 * Code for the Events Calendar feature.
 */

include_once('lcm_events.features.inc');
include_once('lcm_events.date_mapper.inc');

define('JQUERYQTIP_FILENAME', 'jquery.qtip-1.0.0-rc3.min.js');
define('JQUERYQTIP_DOWNLOAD', 'https://github.com/Craga89/qTip1/archive/master.tar.gz');

/**
 * Implements hook_init().
 * @todo Features 7.x-1.0-beta6 contains a bug that strips out added
 * scripts and stylesheets from the .info file. These scripts and 
 * stylesheets should be removed when Features is updated to a later
 * version with the bug fixed. http://drupal.org/node/1335634 -TF
 */
function lcm_events_init() {
  drupal_add_js(drupal_get_path('module', 'lcm_events') . '/js/plugins/jquery.qtip-1.0.0-rc3.min.js');
  drupal_add_js(drupal_get_path('module', 'lcm_events') . '/js/lcm_events_tooltip.js');
}

/**
 * Implements hook_date_formats().
 */
function lcm_events_date_formats() {
  return array(
    array(
      'type' => 'lcm_events-short-month',
      'format' => 'M',
      'locales' => array(),
    ),
    array(
      'type' => 'lcm_events-two-digit-day',
      'format' => 'd',
      'locales' => array(),
    ),
    array(
      'type' => 'lcm_events-time',
      'format' => 'g:ia',
      'locales' => array(),
    ),
    array(
      'type' => 'lcm_events-day-date-time',
      'format' => 'l, F j, Y - g:ia',
      'locales' => array(),
    ),
  );
}

/**
 * Implements hook_date_format_types().
 */
function lcm_events_date_format_types() {
  // This eliminates the need to visit the administrative
  // page in order to assign the format to the format type.
  $lcm_events_date_formats = lcm_events_date_formats();
  foreach($lcm_events_date_formats as $lcm_events_date_format) {
    variable_set('date_format_' . $lcm_events_date_format['type'], $lcm_events_date_format['format']);
  }

  // Define the core date format types.
  return array(
    'lcm_events-short-month' => t('Events Calendar Short Month'),
    'lcm_events-two-digit-day' => t('Events Calendar Two-digit Day'),
    'lcm_events-time' => t('Events Calendar Time'),
    'lcm_events-day-date-time' => t('Events Calendar Day/Date/Time'),
  );
}

/**
 * Implements hook_node_view().
 *
 * Attaches a calendar view of the existing address field,
 * with an info bubble linking to directions on Google Maps.
 */
function lcm_events_node_view($node, $view_mode, $lang_code) {
  if ($node->type == 'event' && $view_mode == 'full' && !empty($node->field_event_location['und'][0]['locality'])) {
    if ($addressfield = $node->field_event_location[LANGUAGE_NONE][0]) {
      $addressparts = array();
      $keys = array('thoroughfare', 'premise', 'locality', 'administrative_area');
      foreach ($keys as $key) {
        if (!empty($addressfield[$key])) {
          $addressparts[] = $addressfield[$key];
        }
      }

      $addressstring = implode(" ", $addressparts);
      $basepath = drupal_get_path('module', 'lcm_events');
      $google_map = array(
        '#markup' => '<div id="gmap-addr">' . $addressstring . '</div>' . '<div id="google-map"></div>',
        '#attached' => array(
          'js' => array(
            "{$basepath}/js/lcm_events_map.js" => array(
              'scope' => 'footer',
            ),
            'https://maps.googleapis.com/maps/api/js?sensor=false' => array(
              'type' => 'external',
            ),
            libraries_get_path('jquery.qtip') . '/' . JQUERYQTIP_FILENAME,
          ),
          'css' => array(
            'style' => "{$basepath}/css/lcm_events_map.css",
          ),
        ),
        '#weight' => 99,
      );
      $node->content['google_map'] = $google_map;
    }
  }
}

function theme_lcm_events_date_all_day_label() {
  return '<span class="all-day">(' . t('All day', array(), array('context' => 'datetime')) .')</span>';
}

function theme_lcm_events_date_all_day($vars) {
  $field    = $vars['field'];
  $instance = $vars['instance'];
  $which    = $vars['which'];
  $date1    = $vars['date1'];
  $date2    = $vars['date2'];
  $format   = $vars['format'];
  $entity   = $vars['entity'];
  $view     = !empty($vars['view']) ? $vars['view'] : NULL;

  if (empty($date1) || !is_object($date1) || $format == 'format_interval') {
    return;
  }
  if (empty($date2)) {
    $date2 = $date1;
  }

  $suffix = '';
  if (!date_has_time($field['settings']['granularity'])) {
    $format = date_limit_format($format, array('year', 'month', 'day'));
  }
  else {
    $format_granularity = date_format_order($format);
    $format_has_time = FALSE;
    if (in_array('hour', $format_granularity)) {
      $format_has_time = TRUE;
    }
    $all_day = date_all_day_field($field, $instance, $date1, $date2);
    if ($all_day && $format_has_time) {
      $format = date_limit_format($format, array('year', 'month', 'day'));
      $suffix = ' ' . theme('date_all_day_label');
    }
  }

  return '<span class="date-wrapper-inner">' . trim(date_format_date($$which, 'custom', $format) . '</span>' . $suffix);

}

/**
 * Implements hook_theme_registry_alter().
 */
function lcm_events_theme_registry_alter(&$theme_registry) {
  // Replace calendar datebox template.
  $theme_registry['calendar_month_col']['path'] = drupal_get_path('module', 'lcm_events') . '/theme';
  $theme_registry['calendar_datebox']['path'] = drupal_get_path('module', 'lcm_events') . '/theme';
  $theme_registry['date_all_day_label']['theme path'] = drupal_get_path('module', 'lcm_events');
  $theme_registry['date_all_day_label']['function'] = 'theme_lcm_events_date_all_day_label';
  $theme_registry['date_all_day']['theme path'] = drupal_get_path('module', 'lcm_events');
  $theme_registry['date_all_day']['function'] = 'theme_lcm_events_date_all_day';
}

/**
 * Implements hook_field_formatter_info().
 */
function lcm_events_field_formatter_info() {
  return array(
    'lcm_events_xml_formatter' => array(
      'label' => t('XML Format'),
      'field types' => array('addressfield'),
    )
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function lcm_events_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $elements = array();
  foreach ($items as $delta => $item) {
    $elements[$delta] = array(
      '#markup' => theme('lcm_events_formatter_' . $display['type'], array('element' => $item,'field' => $instance)),
    );
  }
  return $elements;
}

/**
 * Implements hook_theme().
 */
function lcm_events_theme() {
  return array(
    'lcm_events_formatter_lcm_events_xml_formatter' => array(
      'variables' => array('element' => NULL),
    ),
  );
}

/**
 * Theme function for 'lcm_events_xml' field formatter.
 */
function theme_lcm_events_formatter_lcm_events_xml_formatter($element) {
  $field = $element['field'];
  $element = $element['element'];
  $output = "";
  foreach ($element as $addressfield => $value) {
    $output .= "\n\t";
    $output .= "<" . $addressfield . ">";
    $output .= $value;
    $output .= "</" . $addressfield . ">";
  }
  $output .= "\n";

  return $output;
}