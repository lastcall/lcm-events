<?php

/**
 * Overrides template_preprocess_views_data_export_xml_body() from Views Data Export.
 */
function lcm_events_preprocess_views_data_export_xml_body(&$vars) {
  foreach ($vars['themed_rows'] as $num => $row) {
    foreach ($row as $field => $content) {
      if ($field == 'field_event_location') {
        $content = decode_entities($content);
        $content = preg_replace('/&(?!(amp|quot|#039|lt|gt);)/', '&amp;', $content);
        $vars['themed_rows'][$num][$field] = $content;
      }
    }
  }
}

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see FeedsNodeProcessor::getMappingTargets().
 */
function lcm_events_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);
    if (in_array($info['type'], array('date', 'datestamp', 'datetime'))) {
      $targets[$name . ':lcm_events_start'] = array(
        'name' => t('@name (Events Feeds): Start', array('@name' => $instance['label'])),
        'callback' => 'lcm_events_feeds_set_target',
        'description' => t('The start date for the @name field. Also use if mapping both start and end.', array('@name' => $instance['label'])),
        'real_target' => $name,
      );
      $targets[$name . ':lcm_events_end'] = array(
        'name' => t('@name (Events Feeds): End', array('@name' => $instance['label'])),
        'callback' => 'lcm_events_feeds_set_target',
        'description' => t('The end date for the @name field.', array('@name' => $instance['label'])),
        'real_target' => $name,
      );
    }
  }
}

/**
 * Implements hook_feeds_set_target().
 *
 */
function lcm_events_feeds_set_target($source, $entity, $target, $feed_element) {
  list($field_name, $sub_field) = explode(':', $target, 2);
  if (!($feed_element instanceof LCMEventsDateTimeElement)) {
    if (is_array($feed_element)) {
      foreach ($feed_element as $key => $f) {
        if ($sub_field == 'lcm_events_end') {
          $array_element = new LCMEventsDateTimeElement(NULL, $f);
        }
        else {
          $array_element = new LCMEventsDateTimeElement($f, NULL);
        }
        $array_element->eventsFieldsBuildDateField($entity, $field_name, $key);
      }
    }
    else {
      if ($sub_field == 'lcm_events_end') {
        $feed_element = new LCMEventsDateTimeElement(NULL, $feed_element);
      }
      else {
        $feed_element = new LCMEventsDateTimeElement($feed_element, NULL);
      }
      $feed_element->eventsFieldsBuildDateField($entity, $field_name);
    }
  }
}
