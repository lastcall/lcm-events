api = 2
core = 7.x

libraries[lcm_events][download][type] = "get"
libraries[lcm_events][download][url] = "https://github.com/Craga89/qTip1/archive/master.zip"
libraries[lcm_events][directory_name] = "jquery.qtip"
libraries[lcm_events][destination] = "libraries"
libraries[lcm_events][download][subtree] = "qTip1-master/1.0.0-rc3"