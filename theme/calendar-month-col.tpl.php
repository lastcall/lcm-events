<?php
/**
 * @file
 * Template to display a column
 * 
 * - $item: The item to render within a td element.
 */
$id = (isset($item['id'])) ? 'id="' . $item['id'] . '" ' : '';
$date = (isset($item['date'])) ? ' data-date="' . $item['date'] . '" ' : '';
$day = (isset($item['day_of_month'])) ? ' data-day-of-month="' . $item['day_of_month'] . '" ' : '';
$headers = (isset($item['header_id'])) ? ' headers="'. $item['header_id'] .'" ' : '';
?>
<td <?php print $id?>class="<?php 
  if (!empty($item['all_day']) && ($item['item']->calendar_start_date != $item['item']->calendar_end_date)) {
    print 'multi-day-all-day';
  } elseif (!empty($item['all_day']) && ($item['item']->calendar_start_date == $item['item']->calendar_end_date)) {
    print 'single-day-all-day';
  } else {
    print '';
  }
?> <?php print $item['class'] ?>" colspan="<?php print $item['colspan'] ?>" rowspan="<?php print $item['rowspan'] ?>"<?php print $date . $headers . $day; ?>>
  <div class="inner">
    <?php print $item['entry'] ?>
  </div>
</td>
