<?php
/**
 * @file
 * lcm_events.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function lcm_events_default_rules_configuration() {
  $items = array();
  $items['rules_notify_admin_of_new_event'] = entity_import('rules_config', '{ "rules_notify_admin_of_new_event" : {
      "LABEL" : "Notify admin of new event",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "lcm_events" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "event" : "event" } } } },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[site:mail]",
            "subject" : "[node:field-event-submitter-name] has created a new [node:type] on [site:name]!",
            "message" : "To approve the [node:type], [node:title], go to this link: [node:edit-url].\\r\\n\\r\\nSubmission details:\\r\\n\\r\\nSubmitter\\u0027s name: [node:field-event-submitter-name]\\r\\nSubmitter\\u0027s email: [node:field-event-submitter-email]\\r\\nSubmitter\\u0027s phone: [node:field-event-submitter-phone]",
            "language" : "en"
          }
        }
      ]
    }
  }');
  $items['rules_redirect_users_after_creating_event'] = entity_import('rules_config', '{ "rules_redirect_users_after_creating_event" : {
      "LABEL" : "Redirect users after creating event",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "event" : "event" } } } },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "events-calendar\\/month" } },
        { "drupal_message" : { "message" : "Thank you for submitting an event. It will be posted once it has been reviewed by the administrator." } }
      ]
    }
  }');
  return $items;
}
